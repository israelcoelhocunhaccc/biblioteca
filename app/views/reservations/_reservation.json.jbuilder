json.extract! reservation, :id, :book_id, :librarian_id, :user_id, :active, :created_at, :updated_at
json.url reservation_url(reservation, format: :json)

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
print "Gerando nomes de Autores(Authors) ... "
    Author.create!([{name:"J.K Rowling"},
                {name:"Stephen King"},
                {name:"Charles Dickens"}])
    puts "[OK]"
    puts ""

print "Gerando Categorias(Categories) ... "
    Category.create!([{name:"Fantasia"},
                {name:"Horror"},
                {name:"Romance"}])
    puts "[OK]"
    puts ""

    book_titles=["Harry Potter",
                "It - A Coisa",
                "Os Miseráveis"]

    book_synopses=["Harry Potter é uma série de sete romances 
                    de fantasia escrita pela autora britânica 
                    J. K. Rowling. A série narra as aventuras 
                    de um jovem chamado Harry James Potter, 
                    que descobre aos 11 anos de idade que é 
                    um bruxo ao ser convidado para estudar na 
                    Escola de Magia e Bruxaria de Hogwarts.",
                    "Em 1960, sete pré-adolescentes lutam 
                    contra um demônio maligno que se apresenta 
                    na forma de um palhaço assassino de crianças. 
                    Trinta anos depois, eles se reencontram para 
                    deter o demônio de uma vez por todas quando 
                    ele reaparece na sua cidade natal.",
                    "Na França do século 19, o ex-prisioneiro 
                    Jean Valjean, perseguido ao longo de décadas 
                    pelo impiedoso policial Javert por ter 
                    violado sua liberdade condicional, busca 
                    redenção pelo seu passado e decide acolher a 
                    filha da prostituta Fantine."]

    i=1
    print "Gerando livros(Books) ... "
    3.times do 
        Book.create!(tittle:book_titles[i-1],
                     author:Author.find(i),
                     category:Category.find(i),
                     synopsis:book_synopses[i-1])
        i+=1
    end
    puts "[OK]"
    puts ""

    print "Gerando nomes de usuários(User) ... "
    User.create!([{name: "Israel"},
                  {name: "Israel II"},
                  {name: "Israel III"}])
    puts "[OK]"
    puts ""

    print "Gerando nomes dos bibliotecários(Librarian) ... "
    Librarian.create!([{name: "alsoIsrael", 
                        email:"israelcoelhocunhaccc@gmail.com",
                        password:"123456",
                        password_confirmation:"123456"}, 
                       {name: "alsoIsrael II", 
                        email:"israelcoelhocunhaccc2@gmail.com",
                        password:"123456",
                        password_confirmation:"123456"},
                       {name: "alsoIsrael III", 
                        email:"israelcoelhocunhaccc3@gmail.com",
                        password:"123456",
                        password_confirmation:"123456"}])
    puts "[OK]"
    puts ""

    i=1

    print "Gerando reservas(Reservations) ... "    
    3.times do 
    Reservation.create!(book:Book.find(i),
                        user:User.find(i),
                        librarian:Librarian.find(i),
                        active:true)
    i+=1
    end
    puts "[OK]"
    puts ""

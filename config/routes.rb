Rails.application.routes.draw do

  get '/home' => 'home#index'

  root 'home#index'

  resources :reservations
  resources :books
  resources :categories
  resources :librarians
  resources :librarian
  devise_for :librarian, path: 'routeto'
  resources :users
  resources :authors
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
